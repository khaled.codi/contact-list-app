import { Component } from 'react';
import Contact from './Contact';
import { Transition, animated } from 'react-spring';

export default class ContactList extends Component {
    render() {
        let { contacts, updateContact, deleteContact } = this.props;
        return (
            <Transition
                items={contacts}
                keys={contact => contact.id}
                from={{ transform: "translate3d(-100px,0,0)" }}
                enter={{ transform: "translate3d(0,0px,0)" }}
                leave={{ transform: "translate3d(-100px,0,0)" }}
            >
                {(style, contact) => (
                    <animated.div style={style}>
                        <Contact
                            key={contact.id}
                            id={contact.id}
                            name={contact.name}
                            email={contact.email}
                            image={contact.image}
                            updateContact={updateContact}
                            deleteContact={deleteContact}
                        />
                    </animated.div>
                )}
            </Transition>
        )
    }
}