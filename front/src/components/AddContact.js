import { Component } from 'react';

export default class AddContact extends Component {

    state = { name: "", email: "", image: "" }

    handleSubmit = (event) => {
        event.nativeEvent.preventDefault();
        let { name, email, image } = this.state;
        this.props.createContact({ name, email, image });
    }

    handleChange = (event) => {
        let { name, value } = event.target;
        this.setState({ [name]: value });
    }

    handleChangeField = (event) => {
        let { name, files } = event.target;
        this.setState({ [name]: files[0] });
    }

    render() {
        let { name, email } = this.state;
        return (
            <form onSubmit={this.handleSubmit}>

                <div>
                    <input
                        required
                        type="text"
                        name="name"
                        placeholder="Name"
                        value={name}
                        onChange={this.handleChange}
                    />
                </div>

                <div>
                    <input
                        required
                        type="email"
                        name="email"
                        placeholder="Email"
                        value={email}
                        onChange={this.handleChange}
                    />
                </div>

                <div>
                    <input
                        type="file"
                        name="image"
                        onChange={this.handleChangeField}
                    />
                </div>

                <button type="submit">add contact</button>

            </form>
        )
    }
}