import { Component } from 'react';

export default class Contact extends Component {

    state = {
        edit_mode: false,
        id: this.props.id,
        name: this.props.name,
        email: this.props.email
    }

    toggleEditMode = () => {
        let edit_mode = !this.state.edit_mode;
        this.setState({ edit_mode });
    }

    handleChange = (event) => {
        let { name, value } = event.target;
        this.setState({ [name]: value });
    }

    handleChangeField = (event) => {
        let { name, files } = event.target;
        this.setState({ [name]: files[0] });
    }

    handleSubmit = (event) => {
        event.nativeEvent.preventDefault();
        let { id, updateContact } = this.props;
        let { name, email, image } = this.state;
        updateContact(id, { name, email, image });
        this.toggleEditMode();
    }

    handleReset = (event) => {
        event.nativeEvent.preventDefault();
        let { name, email } = this.props;
        this.setState({ name, email });
        this.toggleEditMode();
    }

    renderEditMode = () => {
        let { name, email } = this.state;
        return (
            <div className="contact_card">
                <form onSubmit={this.handleSubmit} onReset={this.handleReset}>
                    <div>
                        <input
                            required
                            type="text"
                            name="name"
                            placeholder="Name"
                            value={name}
                            onChange={this.handleChange}
                        />
                    </div>
                    <div>
                        <input
                            required
                            type="email"
                            name="email"
                            placeholder="Email"
                            value={email}
                            onChange={this.handleChange}
                        />
                    </div>
                    <div>
                        <input
                            type="file"
                            name="image"
                            onChange={this.handleChangeField}
                        />
                    </div>
                    <button type="reset" className="marginRight">cancel</button>
                    <button type="submit">update contact</button>
                </form>
            </div>
        )
    }

    renderViewMode = () => {
        let { id, name, email, image, deleteContact } = this.props;
        return (
            <div className="contact_card">
                <p>{name} - {email}</p>
                {image && (
                    <img
                        src={`http://localhost:8000/images/${image}`}
                        alt={`the avatar of ${name}`}
                        style={{ width: '100%' }}
                    />
                )}
                <button className="delete marginRight" onClick={() => deleteContact(id)}>x</button>
                <button onClick={this.toggleEditMode}>edit</button>
            </div>
        )
    }

    render() {
        let { edit_mode } = this.state;

        if (edit_mode) return this.renderEditMode();
        else return this.renderViewMode();
    }
}