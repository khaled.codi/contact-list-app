import { Component } from 'react';
import { toast } from 'react-toastify';
import { setCookie, removeCookie } from '../cookie';

export default class Login extends Component {

    state = {
        id: null,
        token: null,
        name: "",
        nickname: "",
        password: "",
    }

    handleChange = (e) => {
        let { name, value } = e.target;
        this.setState({ [name]: value });
    }

    handleSubmit = event => {
        event.preventDefault();
        let { name, password } = this.state;
        if (!name || !password) {
            return toast.error("all fields are required");
        }
        this.login()
    }

    login = async () => {
        const { name, password } = this.state;
        try {
            const url = 'http://localhost:8000/login';
            const body = JSON.stringify({ name, password });
            const headers = { 'Content-Type': 'application/json' };

            const response = await fetch(url, { method: "POST", headers, body });
            const answer = await response.json();
            if (answer.success) {
                // answer.result: id, token, name, nickname
                this.props.onLogin(answer.result);
                toast(`successful login`);
                // now set the cookie
                setCookie('id', answer.result.id, 30);
                setCookie('token', answer.result.token, 30);
            } else {
                this.setState({ error_message: answer.message });
                toast.error(answer.message);
            }
        } catch (err) {
            this.setState({ error_message: err.message });
            toast.error(err.message);
        }
    };

    render() {
        let { name, password } = this.state;
        return (
            <form className="third" onSubmit={this.handleSubmit}>
                <input
                    required name="name" placeholder="name" type="text"
                    value={name} onChange={this.handleChange}
                />
                <input
                    required name="password" placeholder="password" type="password"
                    value={password} onChange={this.handleChange}
                />
                <input type="submit" value="login" />
            </form>
        )
    }

}