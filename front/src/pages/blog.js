import { Component } from "react";
import { Link } from "react-router-dom";

export default class Blog extends Component {
    render() {
        let id = this.props.match.params.id;
        return (
            <div>
                <p>this is the blog page {id}</p>
                <Link to="/">go back to home</Link>
            </div>
        )
    }
}