import { Component } from 'react';
import { toast } from 'react-toastify';
import { setCookie, removeCookie } from '../cookie';

export default class Signup extends Component {

    state = {
        id: null,
        token: null,
        name: "",
        nickname: "",
        password: "",
    }

    handleChange = (e) => {
        let { name, value } = e.target;
        this.setState({ [name]: value });
    }

    handleSubmit = event => {
        event.preventDefault();
        let { name, password } = this.state;
        if (!name || !password) {
            return toast.error("all fields are required");
        }
        this.signup()
    }

    signup = async () => {
        const { name, nickname, password } = this.state;
        try {
            const url = 'http://localhost:8000/signup';
            const body = JSON.stringify({ name, nickname, password });
            const headers = { 'Content-Type': 'application/json' };

            const response = await fetch(url, { method: "POST", headers, body });
            const answer = await response.json();
            if (answer.success) {
                // answer.result: id, token, name, nickname
                let user = answer.result;
                this.props.onLogin(user);
                toast(`successful signup`);
                // now set the cookie
                setCookie('id', user.id, 30);
                setCookie('token', user.token, 30);
            } else {
                this.setState({ error_message: answer.message });
                toast.error(answer.message);
            }
        } catch (err) {
            this.setState({ error_message: err.message });
            toast.error(err.message);
        }
    };

    render() {
        let { name, nickname, password } = this.state;
        return (
            <form className="third" onSubmit={this.handleSubmit}>
                <input
                    required name="name" placeholder="name" type="text"
                    value={name} onChange={this.handleChange}
                />
                <input
                    required name="nickname" placeholder="nickname" type="text"
                    value={nickname} onChange={this.handleChange}
                />
                <input
                    required name="password" placeholder="password" type="password"
                    value={password} onChange={this.handleChange}
                />
                <input type="submit" value="signup" />
            </form>
        )
    }

}