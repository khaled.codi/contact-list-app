import { Component } from 'react';
import AddContact from '../components/AddContact';
import ContactList from '../components/ContactList';
import { Link } from "react-router-dom";
import { toast } from 'react-toastify';
import { pause } from '../utils';

export default class Home extends Component {

  state = {
    contacts: [],
    error: false,
    loading: false
  }

  async componentDidMount() {
    await this.getContacts();
  }

  getContacts = async () => {
    try {
      const response = await fetch('//localhost:8000/contacts');
      const result = await response.json();
      if (result.success) {
        const contacts = result.result;
        this.setState({ contacts });
      } else {
        const error = result.message;
        this.setState({ error });
      }
    } catch (err) {
      this.setState({ error_message: err })
    }
  }

  getContact = async id => {
    try {
      const response = await fetch(`//localhost:8000/contacts/${id}`);
      const result = await response.json();
      if (result.success) {
        const contact = result.result;
        this.setState({ contact });
      } else {
        const error = result.message;
        this.setState({ error });
      }
    } catch (err) {
      this.setState({ error_message: err })
    }
  }

  deleteContact = async id => {
    try {

      const response = await fetch(`//localhost:8000/contacts/delete/${id}`);
      const result = await response.json();

      if (result.success) {

        let stateContacts = [...this.state.contacts].filter(contact => contact.id !== id);
        this.setState({ contacts: stateContacts });

      } else this.setState({ error: result.message });

    } catch (err) {
      this.setState({ error_message: err })
    }
  }

  updateContact = async (id, params) => {
    let { name, email, image } = params;
    let url = `//localhost:8000/contacts/update/${id}`;
    let paramsErr = "you need at least name or email properties to update a contact";

    // create our url
    if (!name && !email) throw new Error(paramsErr);
    else if (name && !email) url += `/?name=${name}`;
    else if (!name && email) url += `/?email=${email}`;
    else if (name && email) url += `/?name=${name}&email=${email}`;

    let body = null;
    if (image) {
      body = new FormData();
      body.append(`image`, image);
    }

    try {

      const response = await fetch(url, { method: "POST", body });
      const result = await response.json();

      if (result.success) {

        let stateContacts = [...this.state.contacts].map(contact => {
          if (contact.id !== id) return contact;
          return {
            id,
            name: name || contact.name,
            email: email || contact.email
          }
        });

        this.setState({ contacts: stateContacts });

      } else this.setState({ error: result.message });

    } catch (err) {
      this.setState({ error_message: err })
    }
  }

  createContact = async (params = {}) => {
    let { name, email, image } = params;
    let url = `//localhost:8000/contacts/create/?name=${name}&email=${email}`;
    let paramsErr = "you need at least name or email properties to update a contact";
    if (!name || !email) throw new Error(paramsErr);

    let body = null;
    if (image) {
      body = new FormData();
      body.append(`image`, image);
    }

    try {
      this.setState({ loading: true });

      await pause();
      const response = await fetch(url, { method: "POST", body });
      const result = await response.json();

      this.setState({ loading: false });

      if (result.success) {

        let newContact = result.result;
        let id = newContact.id;
        let stateContacts = [...this.state.contacts];

        stateContacts.push({ id, name, email, image: newContact.name });

        this.setState({ contacts: stateContacts, name: "", email: "" });
        toast("Contact added");

      } else {
        toast.error("Cannot add contact");
        this.setState({ error: result.message });
      }

    } catch (err) {
      this.setState({ error_message: err })
    }
  }

  clearError = () => {
    this.setState({ error: false })
  }

  navigateToContact = () => {
    this.props.history.push('/contact')
  }

  render() {
    let { contacts, error, loading } = this.state;

    if (loading) return <p>loading...</p>

    return (
      <>

        {error ? (
          <div>
            <p>{error}</p>
            <button onClick={this.clearError}>refresh</button>
          </div>
        ) : (
          <div className="App">

            <AddContact
              createContact={this.createContact}
            />

            <ContactList
              contacts={contacts}
              updateContact={this.updateContact}
              deleteContact={this.deleteContact}
            />

            <div>
              <Link to="/contact">go to contact</Link>
            </div>
            <button onClick={this.navigateToContact}>
              click this button to go to contact
            </button>

          </div>
        )}

      </>
    )
  }

}