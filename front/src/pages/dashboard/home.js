import { Component } from "react";
import { Link } from "react-router-dom";

export default class DashboardHome extends Component {
    render() {
        return (
            <div>
                <p>this is the dashboard home</p>
                <Link to="/">go back to home</Link>
            </div>
        )
    }
}