import { Component, Fragment } from 'react';
import { ToastContainer } from 'react-toastify';
import { withRouter, Switch, Route, Redirect } from "react-router-dom";
import Home from './pages/home';
import Login from './pages/login';
import Signup from './pages/signup';
import Contact from './pages/contact';
import Blog from './pages/blog';
import Header from './components/Header';
import DashboardHome from './pages/dashboard/home';
import { getCookie } from './cookie';
import 'react-toastify/dist/ReactToastify.css';
import './App.css';

class App extends Component {

  state = {
    user: {
      id: null,
      admin: false
    }
  }

  async componentDidMount() {
    let id = getCookie('id');
    let token = getCookie('token');
    if (id && token) {
      let headers = { 'Content-Type': 'application/json', id, token };
      let response = await fetch('//localhost:8000/getUserData', { headers });
      let data = await response.json();
      if (data.success) {
        this.setState({ user: data.result });
      }
    }
  }

  onLogin = (user) => {
    this.setState({ user });
  }

  onLogout = () => {
    this.setState({ user: { token: null } });
  }

  render() {
    let { user } = this.state;
    let admin = user.admin;
    return (
      <Fragment>

        <Header
          user={user}
          onLogout={this.onLogout}
        />

        <Switch>
          <Route path="/" component={props => <Home {...props} user={user} />} exact />
          <Route path="/signup" render={props => {
            return user.token ? <Redirect {...props} to="/" /> : (
              <Signup
                {...props}
                user={user}
                onLogin={this.onLogin}
              />
            )
          }} />
          <Route path="/login" render={props => {
            return user.token ? <Redirect {...props} to="/" /> : (
              <Login
                {...props}
                user={user}
                onLogin={this.onLogin}
              />
            )
          }} />
          <Route path="/contact" component={Contact} />
          <Route path="/blog/:id" component={Blog} />
          <Route path="/dashboard">
            {admin ? <DashboardHome /> : <Redirect to="/" />}
          </Route>
        </Switch>

        <ToastContainer />

      </Fragment>
    )
  }

}

export default withRouter(App);