// back/src/db.js
const sqlite = require("sqlite");
const sqlite3 = require("sqlite3");
const SQL = require("sql-template-strings");

/**
 * 
 * Joins multiple statements. Useful for `WHERE x = 1 AND y = 2`, where the number of arguments is variable.
 * 
 * Usage:
 * 
 * joinSQLStatementKeys( ["name", "age", "email"], { email:"x@y.c", name="Z"}, ", ")
 * 
 * 
 * Will return an SQL statement corresponding to the string:
 * 
 * name="Z", email="x@y.c"
 * 
 * 
 * @param {Array} keys an array of strings representing the properties you want to join 
 * @param {Object} values an object containing the values 
 * @param {string} delimiter a string to join the parts with
 * @param {string} keyValueSeparator a string to join the parts with
 * @returns {Statement} an SQL Statement object
 */
const joinSQLStatementKeys = (keys, values, delimiter, keyValueSeparator = '=') => {
    return keys
        .map(propName => {
            const value = values[propName];
            if (value !== null && typeof value !== "undefined") {
                return SQL``.append(propName).append(keyValueSeparator).append(SQL`${value}`);
            }
            return false;
        })
        .filter(Boolean)
        .reduce((prev, curr) => prev.append(delimiter).append(curr));
};

const initializeDatabase = async () => {

    const db = await sqlite.open({
        filename: 'db.sqlite',
        driver: sqlite3.Database
    });

    db.migrate({ force: 'last' });

    /**
     * get all the actions from auth controller
     */
    const authActions = require('./auth/controller')(db);

    /**
     * retrieves the contacts from the database
     * @param {string} orderBy an optional string that is either "name" or "email"
     * @returns {array} the list of contacts
     */
    const getContactsList = async (orderBy) => {
        let statement = `SELECT contact_id AS id, name, email, image FROM contacts`
        switch (orderBy) {
            case 'name': statement += ` ORDER BY name`; break;
            case 'email': statement += ` ORDER BY email`; break;
            default: break;
        }
        try {
            return await db.all(statement);
        } catch (e) {
            throw new Error(`couldn't retrieve contacts: ` + e.message);
        }
    }

    const getContact = async (id) => {
        let statement = `SELECT contact_id AS id, name, email, image FROM contacts WHERE contact_id = ${id}`
        const contact = await db.get(statement);
        if (!contact) throw new Error(`contact ${id} not found`);
        return contact;
    }

    const createContact = async (props) => {
        if (!props || !props.name || !props.email) {
            throw new Error(`you must provide a name and an email`);
        }
        const { name, email, image } = props;
        try {
            const result = await db.run(`INSERT INTO contacts (name,email,image) VALUES (?, ?, ?)`, [name, email, image]);
            const id = result.lastID;
            return id;
        } catch (e) {
            throw new Error(`couldn't insert this combination: ` + e.message);
        }
    }

    const deleteContact = async (id) => {
        try {
            const result = await db.run(`DELETE FROM contacts WHERE contact_id = ?`, id);
            if (result.changes === 0) throw new Error(`contact "${id}" does not exist`);
            return true;
        } catch (e) {
            throw new Error(`couldn't delete the contact "${id}": ` + e.message);
        }
    }

    const updateContact = async (id, props) => {

        if (!props && !(props.name && props.email)) {
            throw new Error(`you must provide a name or an email`);
        }

        const statement = SQL`UPDATE contacts SET `
            .append(
                joinSQLStatementKeys(
                    ["name", "email", "image"],
                    props,
                    ", "
                )
            )
            .append(SQL` WHERE `)
            .append(
                joinSQLStatementKeys(
                    ["contact_id"],
                    { contact_id: id },
                    " AND "
                )
            );

        try {
            const result = await db.run(statement);
            if (result.changes === 0) throw new Error(`no changes were made`);
            return true;
        } catch (e) {
            throw new Error(`couldn't update the contact ${id}: ` + e.message);
        }
    }

    const controller = {
        getContactsList,
        getContact,
        createContact,
        deleteContact,
        updateContact,
        ...authActions
    }

    return controller;
}

const db = { joinSQLStatementKeys, initializeDatabase }

module.exports = db;