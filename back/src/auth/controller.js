const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

function controller(db) {

    async function isLoggedIn(req, res, next) {

        const id = req.header("id");
        const token = req.header("token");

        if (!token) next(new Error("Auth Error"));
    
        try {
    
            const decoded = jwt.verify(token, "randomString");
            if (id != decoded.userId) next(new Error("Invalid Token"));

            const statement = `SELECT user_id AS id, name, nickname, token FROM users WHERE token = "${token}"`;
            const user = await db.get(statement);
            if (!user || !user.id || user.id != id) next(new Error("Invalid Token"));

            req.userId = decoded.userId;
            req.user = user;
            
            next();
    
        } catch (e) {
            next(new Error("Invalid Token"));
        }
        
    }

    async function signupAction({ name, nickname, password }) {
        // check body data
        if (!name || !password) throw new Error("Email and password are required");
        try {
            // check if user already exists or not
            let selectStmt = `SELECT user_id AS id, name, nickname, password FROM users WHERE name = "${name}"`;
            let user = await db.get(selectStmt);
            if (user) throw new Error("User already exists");

            // hash password
            let salt = await bcrypt.genSalt(10);
            let hashedPassword = await bcrypt.hash(password, salt);

            let insertStmt = 'INSERT INTO users (name, nickname, password) VALUES (?, ?, ?)';
            let result = await db.run(insertStmt, [name, nickname, hashedPassword]);
            let id = result && result.lastID;

            // generate token
            let payload = { userId: id };
            let token = jwt.sign(payload, "randomString", { expiresIn: 10000 });

            // add token to the user
            await db.run('UPDATE users SET token = ? WHERE user_id = ?', token, id);

            return { id, token, name, nickname };
        } catch (e) {
            throw new Error(`couldn't create user ` + e.message);
        }
    }

    async function loginAction({ name, password }) {
        // check body data
        if (!name || !password) throw new Error("Email and password are required");
        try {
            // get user
            let statement = `SELECT user_id AS id, name, nickname, password FROM users WHERE name = "${name}"`;
            let user = await db.get(statement);
            if (!user) throw new Error("User not found");

            // check the password
            let isMatch = await bcrypt.compare(password, user.password);
            if (!isMatch) throw new Error("Incorrect Password !");

            // generate token
            let payload = { userId: user.id };
            let token = jwt.sign(payload, "randomString", { expiresIn: 10000 });

            // add token to the user
            await db.run('UPDATE users SET token = ? WHERE user_id = ?', token, user.id);

            return { ...user, token };
        } catch (e) {
            throw new Error(`couldn't login user ` + e.message);
        }
    }

    async function logoutAction(userId) {
        try {
            // remove token for user record
            await db.run('UPDATE users SET token = ? WHERE user_id = ?', null, userId);
            return { message: "logged out successfully" }
        } catch (e) {
            throw new Error(`couldn't logout user ` + e.message);
        }
    }

    return { isLoggedIn, signupAction, loginAction, logoutAction }

}

module.exports = controller;