// back/src/index.js
const app = require('./app');
const db = require('./db');
const path = require("path");
const multer = require('multer');

const start = async () => {

    /** uploader */
    const multerStorage = multer.diskStorage({
        destination: path.join(__dirname, '../public/images'),
        filename: (req, file, cb) => {
            const { fieldname, originalname } = file;
            const date = Date.now();
            // filename will be: image-1345923023436343-filename.png
            const filename = `${fieldname}-${date}-${originalname}`;
            cb(null, filename);
        }
    })
    const upload = multer({ storage: multerStorage })

    /** wait for the controller */
    const controller = await db.initializeDatabase();

    /** get all the auth routes: login - signup */
    require('./auth/routes')(app, controller);

    /** all the contacts routes */

    app.get('/', (req, res) => res.send("ok"));

    // CREATE
    app.post('/contacts/create', upload.single('image'), async (req, res, next) => {
        const { name, email } = req.query;
        const image = req.file && req.file.filename;
        try {
            const result = await controller.createContact({ name, email, image });
            res.json({ success: true, result });
        } catch (e) {
            next(e);
        }
    })

    // LIST
    app.get('/contacts', async (req, res, next) => {
        const { orderBy } = req.query
        try {
            const contacts = await controller.getContactsList(orderBy);
            res.json({ success: true, result: contacts });
        } catch (e) {
            next(e);
        }
    })

    // READ SINGLE
    app.get('/contacts/:id', async (req, res, next) => {
        const { id } = req.params
        try {
            const contact = await controller.getContact(id);
            res.json({ success: true, result: contact });
        } catch (e) {
            next(e);
        }
    })

    // DELETE
    app.get('/contacts/delete/:id', async (req, res, next) => {
        const { id } = req.params
        try {
            const result = await controller.deleteContact(id);
            res.json({ success: true, result });
        } catch (e) {
            next(e);
        }
    })

    app.post('/contacts/update/:id', upload.single('image'), async (req, res, next) => {
        try {
            const { id } = req.params;
            const { name, email } = req.query;
            const image = req.file && req.file.filename;
            const result = await controller.updateContact(id, { name, email, image });
            res.json({ success: true, result });
        } catch (e) {
            next(e);
        }
    })

    app.use((err, req, res, next) => {
        res.status(500).json({ success: false, message: err.message });
    })

}

start();

app.listen(8000, () => console.log('server listening on port 8000'));