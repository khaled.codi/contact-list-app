--------------------------------------------------------------------------------
-- Up
--------------------------------------------------------------------------------
ALTER TABLE contacts RENAME TO old_contacts;

CREATE TABLE contacts (
    contact_id INTEGER PRIMARY KEY AUTOINCREMENT,
    name TEXT,
    email TEXT,
    user_id TEXT,
    date TEXT,
    image TEXT,
    FOREIGN KEY(user_id) REFERENCES users(user_id)
);

-- copy old data in new table
-- all previous contacts are for the first user
INSERT INTO contacts (contact_id, name, email, date, user_id) SELECT contact_id, name, email, date, user_id FROM old_contacts;

-- remove previous table
DROP TABLE old_contacts;

--------------------------------------------------------------------------------
-- Down
--------------------------------------------------------------------------------
ALTER TABLE contacts RENAME TO old_contacts;

CREATE TABLE contacts (
    contact_id INTEGER PRIMARY KEY AUTOINCREMENT,
    name TEXT,
    email TEXT,
    user_id TEXT,
    date TEXT,
    FOREIGN KEY(user_id) REFERENCES users(user_id)
);

-- delete new contacts:
DELETE FROM old_contacts WHERE user_id = "fakeId";

-- copy old data to previous table
INSERT INTO contacts (contact_id, name, email, date, user_id) SELECT contact_id, name, email, date, user_id FROM old_contacts;

DROP TABLE old_contacts;