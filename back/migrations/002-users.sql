--------------------------------------------------------------------------------
-- Up
--------------------------------------------------------------------------------
CREATE TABLE users (
    user_id INTEGER PRIMARY KEY AUTOINCREMENT,
    name TEXT UNIQUE,
    nickname TEXT,
    password TEXT,
    token TEXT
);

INSERT INTO users (name, nickname, password)
VALUES ("a", "a", "$2a$10$9CfvvHzBFAMDZhXSSnKUfuJrCbZE7ZlHrNLSFGwnw9I2NqEQN4EgO");

INSERT INTO users (name, nickname, password)
VALUES ("b", "b", "$2a$10$9CfvvHzBFAMDZhXSSnKUfuJrCbZE7ZlHrNLSFGwnw9I2NqEQN4EgO");

ALTER TABLE contacts
    RENAME TO old_contacts;
CREATE TABLE contacts (
    contact_id INTEGER PRIMARY KEY AUTOINCREMENT,
    name TEXT,
    email TEXT,
    date TEXT,
    user_id INTEGER,
    FOREIGN KEY(user_id) REFERENCES users(user_id)
);
-- copy old data in new table
-- all previous contacts are for the first user
INSERT INTO contacts (contact_id, name, email, date, user_id)
SELECT contact_id,
    name,
    email,
    "2010-10-10 10:10:10.000",
    1
FROM old_contacts;
-- remove previous table
DROP TABLE old_contacts;
INSERT INTO contacts (name, email, date, user_id)
VALUES (
        "Patrick Bateman",
        "pat@batman.io",
        "2009-10-10 10:10:10.000",
        2
    );
INSERT INTO contacts (name, email, date, user_id)
VALUES (
        "Hannibal Lecter",
        "hani@food.org",
        "2011-10-10 10:10:10.000",
        2
    );
--------------------------------------------------------------------------------
-- Down
--------------------------------------------------------------------------------
DROP TABLE users;
ALTER TABLE contacts
    RENAME TO old_contacts;
CREATE TABLE contacts (
    contact_id INTEGER PRIMARY KEY AUTOINCREMENT,
    name TEXT,
    email TEXT
);
-- delete new contacts:
DELETE FROM old_contacts
WHERE user_id = 2;
-- copy old data to previous table
INSERT INTO contacts (contact_id, name, email)
SELECT contact_id,
    name,
    email
FROM old_contacts;
DROP TABLE old_contacts;